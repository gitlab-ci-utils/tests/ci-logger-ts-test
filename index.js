const {
    log,
    getLogEntry,
    resetLogEntryDefaults,
    setLogEntryDefaults,
    LogEntry,
    Levels
} = require('ci-logger');

const logEntry = {
    message: 'foo',
    level: Levels.Error,
    exitOnError: false
};
log(logEntry);

const logEntry2 = getLogEntry({ message: 'bar' });
log(logEntry2);

const newDefaults = { isResult: true, level: 'warn' };
setLogEntryDefaults(newDefaults);

const logEntry3 = getLogEntry({ message: 'baz' });
log(logEntry3);
log(logEntry);

resetLogEntryDefaults();

log(logEntry);
