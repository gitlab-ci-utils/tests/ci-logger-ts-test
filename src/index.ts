import {
    log,
    getLogEntry,
    resetLogEntryDefaults,
    setLogEntryDefaults,
    LogEntry,
    LogEntrySettings
} from 'ci-logger';

const logEntry: LogEntry = {
    message: 'foo',
    level: 'error',
    exitOnError: false
};
log(logEntry);

const logEntry2: LogEntry = getLogEntry({ message: 'bar' });
log(logEntry2);

const newDefaults: LogEntrySettings = { isResult: true, level: 'warn' };
setLogEntryDefaults(newDefaults);

const logEntry3: LogEntry = getLogEntry({ message: 'baz' });
log(logEntry3);
log(logEntry);

resetLogEntryDefaults();

log(logEntry);
